import mlflow
import pickle

with mlflow.start_run() as run:
    filename = "model_final.pkl"
    model = pickle.load(open(filename, "rb"))
    mlflow.log_model(model, 'test_model')
